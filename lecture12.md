## Quiz        

1. The routing algorithms
    - It is the end to end path through the network.      
    - It is to find the least cost path.      

2. The forwarding tables
    - It is the local forwarding at the router.      

3. The **global algorithms**
    - It is that all routers know the entire topology. They know what the topology is. They know links between the nodes in the topology.      
    - It is the link state algorithm
        - The link state is that every router know the status of every single link.      

4. The **decentralized algorithms**
    - It is that we determine the least cost link on the **iterative process**.      
        - The **iterative process** is that we compute the cost to our neighbors and we set up the **distance vector**. The neighbors compute the cost to their neighbors and they set up the **distance vector**. Thereafter, they exchange the **distance vector** with us. We repeat the process. So everyone in the network exchange the **distance vectors** with everyone else.      
    
5. The idea of the **global algorithm** is that if something changes with the topology, everybody knows it as soon as possible. It is the global algorithm and it is the broadcast. It is not true for the **decentralized distance vector algorithm**. You will only notify your neighbors. You will only receive update from your neighbors. It is like one step at a time.      

6. The **static route algorithms**
    - It is that the route changes slowly over time.      
    - It is the route that is configured manually. So it stays the same until somebody changes it.      

7. The **dynamic route algorithms**
    - It is that the route change quickly.      
    - We update the routers in the network based on the update schedule in the system.      

8. The **link state** routing algorithm
    - The dijkstra
        - The link state broadcast
            - Every router know the status of every single link.      
        - All the router receive the same info.      

9. The **distance vector** algorithm
    - The bellman ford equation
    - The key idea
        - The node send distance vector to neighbors
        - After it receive the neighbor's distance vector, it use the bellman ford equation to update its own distance vector
        - It will converge and every node in the network will arrive at the same decision
    - It is the **iterative process**
    - It is distributed
        - You will only notify your directly attached neighbors. You will receive the update from your directly attached neighbor.        
    - The node
        - wait
        - recompute
        - notify
    - <img src="img/bellman ford equation.png" alt="bellman ford equation" width="500" height="600">

10. The link state does not compute the actual path, it will only compute their own table. If you advertise and exchange the distance vectors between routers, it is the actual path.      

11. The distance vector link cost change
    - The link cost change
    - The poisoned reverse
        - It indicate that the path is not available        
        - The gateway is not connected
    - <img src="img/link cost change.png" alt="link cost change" width="500" height="600">        
    - <img src="img/poisoned reverse.png" alt="poisoned reverse" width="500" height="600">

12. The hierarchical routing        
    - all router identical
    - network flat
    - administrative autonomy
        - There is no one single organization on the Internet that controls everything. If you own the equipment, you can do whatever you want if you think it is appropriate with the equipment and if you think it is the best option for you.        

13. The autonomous system
    - It is that the aggregate router into region. The administrative entity control the entire network.      
    - The router run the same routing protocol.      
    - The gateway router
        - If you want to get off of your autonomous system to the other autonomous system, you have to go through the Gateway router.      
    - The intra AS and the inter AS.      
        - It is not necessarily a cost type of analysis.      
        - There are a lot of politics.      
    - The hot potato routing
        - It is to make the routing decision that gets the data off the network as fast as possible.      
        - It is the least cost path to send it to the gateway. It's the least cost path to send to the border router to get the data off the network. It is not the least cost path to send it to the destination.      
























































