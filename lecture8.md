## Quiz      

1. The pipeline
    - If we send one data packet and one acknowledgement at a time, we cannot push data fast enough and we cannot take full advantage of the capacity of the throughput. So we will use the pipeline.  
    - The pipeline
        - The pipeline is that in order to take full advantage of the capacity of that medium, we will send more packets than we have actually had acknowledged.  
        - The pipeline is that we're going to send multiple packets all at once, and we're going to continue sending these packets and just assume that everything is going to get there correctly. So we will use the in flight packet. 
    - The in flight packet is the packet that is actively in transit. we're going to begin to send subsequent packets of the data stream. Before the packet arrive at the in system and we're going to continue doing that until we get some sort of notification that something has gone wrong.   
    - With pipeline, you will loss multiple packet at once. 

2. The mechanism to recover from data loss
    - The **go back n** protocol
        - The sender is allowed to transmit multiple packets without waiting for an acknowledgment, but is constrained to have no more than some maximum allowable number, N, of unacknowledged packets in the pipeline.
        - The go back n utilize the **cumulative acknowledgement**. The receiver only send the **cumulative acknowledgement**.  
        - The **cumulative acknowledgement** is that anytime you send an acknowledgement, you are not only acknowledging the packet that you just received, but also every packet in the sequence ahead of it.
        - If there is a gap in the sequence, we will not acknowledge anything after the **gap**.   
        - The sender has an A timer for the oldest unacknowledged packet. If that timer expires, you retransmit every **unacknowledged packet** at once. You retransmit all the **send window** at once. 
        - The **go back n** does not need to **buffer** things at the receipt. 
    - The selective repeat protocol
        - The receiver has to keep what it received **out of order** while it is waiting on the sender to retransmit that one packet. 
        - The **out of order** is that no receiver buffering. 
        - How the receiver handle the **out of order**
            - It does not say, but it drop data, it receive data out of order. 

3. The **cumulative acknowledgement** and the **individual acknowledgement**

4. The TCP actually uses **go back n**
    - TCP uses **go back n** just because of the receiver side buffering. It simplifies the receiver side buffering significantly. 

4. The TCP is **point to point protocol**, **one to one data transmission**, **one sender, one receiver**, **unicast**. 
    - It is **unicast**, it is not **multicasting** and **broadcasting**. 
    - The multicasting
        - It is the one to many data transmission. It is one sender to multiple receivers. 
        - The distributed system 
        - The multimedia. If you want to push the video to multiple receivers at the same time, the TCP cannot do it.  
    - The broadcasting
        - It is the one to all data transmission. It is the one sender to every receiver.   

5. The TCP is reliable, in order

6. The sequence number
    - Each one of those segments is related to this segment that came before it, and the segment that came after it. It is the sequence numbers. 
    - In order to ensure that it's going to put the **data stream** in the order before it delivers to the application, we need those sequence number.
    - We pick the sequence number at random.   

7. The pipeline
    - we're going to use the pipeline to attempt to **use as much of the throughput as possible** on the medium. 

8. TCP is the full duplex
    - The full duplex is that if we are going to have data going back and forth between the sender and the receiver, it is not one way communication, it is the bi **directional connection**. So the sender and the receiver can have a dialogue going back. They can actually be communicating data back and forth. They can do that on the same TCP connection.  
    - If we open one TCP connection, the sender can send data to the receiver and the receiver can use the same TCP to send the data back to the sender. 
    - It is not acknowledgements
        -  The acknowledgements are just receipts that you got something correctly. 

9. The TCP is the connection oriented
    - The handshaking. 
    - In the TCP, you should set thing up. 
    - In the UDP, you do not set thing up. 

10. The **TCP congestion control** is not to overwhelm the network.

11. The **TCP flow control** is not to overwhelm the receiver. 
    - The TCP will ensure that the sender doesn’t send more data than the receiver can buffer. 
    - It is possible that we can push data all the way across the network. That data is received correctly by the transport layer, but the application is actually not ready for it. In order to ensure that the recevier will not drop the data we send to, we will use the receive window. It is the receiver tell the sender that it cannot receive the packet before overflow.  

12. The TCP header
    - The source port number and the destination port number
        - The first thing that after the IP is the source port number and the destination port number. 
        - It is embedded and in IP packet or an IP datagram. So it fit immediately after the IP. 
        - So we use the **source port number** and the **destination port number** to target the **process** on the end system. 
    - The sequence number
        - It is the 32 bit. 
    - The acknowledgement number
    - The header length
        - The option maybe present. 
    - The receive window 
        - The receive window is the number of bytes the receiver want to accept. 
        - It is possible that we can push data all the way across the network. That data is received correctly by the transport layer, but the application is not ready for it. In order to ensure that the recevier will not drop the data we send to, we will use the receive window. 
        - The receive window is that the receiver tell the sender it cannot receive the packet before overflow. 
    - The checksum
        - It is the error detection. It is not the error correction. 
            - If TCP detect the error, it will drop the segment, but it is not going to send an acknowledgement for that segment, so the TCP will retransmit it. 
            - The UDP only drop the segment, it does not retransmit it. 
    - The urgent data pointer
        - It is the interrupt request.      

13. The sequence number
    - The first byte in the segment
    - It does not start at 0. It start at the arbitrary number. 

14. The acknowledgement number
    - The acknowledgement is the next byte in that sequence that you are expecting to receive. 

15. Telnet 
    - It allows you to input the character and receive the echo of that character. 

16. The TCP timeout
    - The short
        - The problem is the premature timeout. It will indicate there is problem whe nothing is wrong. 
    - The long
        - it takes long time to identify the problem. 
    - The timeout interval is not the fixed.      
















    
    
















