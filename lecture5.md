## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1zHbfUagR8HhhKyWCARhpxSkyZAw_9qXT/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1D1KHQMO618ahyRuD8P-ocaPDP_9UXfQS/preview" width="640" height="480"></iframe>

<iframe src="https://drive.google.com/file/d/1Lhn6YN6oZHBfLHJWrv4p7OA1G0CQKeS3/preview" width="640" height="480"></iframe>

## Quiz      

1. The DNS        
    - It translate the web name to the IP address.   
    - It is the aliasing.        
    - The email server aliasing.    
    - The load distribution.      
    - The root DNS server does not track the whole Internet.      
    - The root DNS server track a copy of the top level domain server.        

2. The root DNS server
    - There are 13 root DNS server address in the DNS.      

3. The edge
    - The work is done at the edge. It is the end system.        

4. The DNS is not centralized
    - If we only have one DNS server and there is something wrong with the server, the service on the server is not going to work. We have to wait for the DNS server to get back.
    - If we have only one DNS server, there is a lot of traffic is going to go into the DNS server. The traffic link is going to be overloaded and the system is going to be overloaded. It will not be able to handle all the traffic at once. It will increase congestion.
    - Physical distance. It will not close to every user.
    - Maintenance. It is not easy to be fixed.  
    - It does not scale.        

5. The top level domain server
    - It track the list of authoritative server.      

6. The authoritative server
    - When the IP address updated, it is updated at the authoritative server.       

7. The IP anycast
    - There is not 13 server, there is 13 IP address that to 900 different server. 
    - The anycast, if you go to one server, it will point to some system in particular set.      

8. The local DNS
    - it is in the ISP.   
    - The hierarchy is varies. It has no specific position.        

9. The iterative query and the recursive query.        

10. The DNS is not working for the mobile node.      

11. The DNS record
    - The type

12. The inserting record into the DNS
    - You will provide the primary and the secondary name.      
        - You would insert an NS record, which is going to give the domain name and a link to the host name of the authoritative DNS server.      
        - you would insert a type A record which is giving the IP address for the actual host name of the authoritative server.            

13. The DDoS






















