## Lecture      

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1IE8CrSoywmFhBJAN6ZWS__5j215xViXd/preview"></iframe>

## Quiz      

1. The interior gateway protocols        
    - We use it to move information around inside of our autonomous systems.      

2. The intra As routing
    - The routing information protocol
    - The open shortest path first
    - The interior gateway routing protocol 

3. The distance vector
    - The distance metric
        - The link has a cost of 1
        - the cost in this context is specifically the number of hops
    - Routers exchange distance vectors with their neighbors every 30 seconds. The exchanges is the advertisements
    - Every advertisement can include path destination up to 25 different subnets

4. The routing information protocol link failure recovery
    - IP is supposed to update every 30 seconds. If a router hasn't received an update in 180 seconds, then the link is dead
    - The poison reverse is to prevent the ping pong loop

5. The IP send requests to one another over UDP        
    - The advertisement sent in the UDP packet

6. The forwarding and the routing
    - The routing determines that shortest path through the network based on the routing algorithm. That's how we set up our actual forwarding table      
    - The forwarding doesn't pay any attention to transport protocols

7. The route d
    - The routing table managed by application level process

8. The open shortest path first
    - It is that the implementation of the protocol is publicly available
    - It is the link state algorithm. It use the Dijkstra's algorithm
        - The link state protocol is that we need to have each router broadcast to the entire network
    - The cost can be measured in all sorts of different way. The open shortest path first can use whatever cost metric you want
    - Routers flood the network with link information, so every router has a complete view of the autonomous system. So you flood these advertisements to the entire autonomous system
    - The cost is configured by the network administrator
    - the open shortest path first bundle directly into IP. It does not use the UDP and TCP transport protocol. It is the layer 3 protocol
    - The **is is routing protocol** is closely related to the **open shortest path first**
        - Making sure that every single router in that is is routing has your routing information. It will increase the overhead
    - The security
        - The IP is the old protocol. There is no security in it        
        - There is security in the open shortest path first
    - The open shortest path first allow for multiple same cost paths
        - It is that the packets can take a different path across the network
    - The open shortest path first allow different cost metrics for different types of service 
        - The TOS, type of service. In the United States we have network neutrality that have implications. So we divide traffic up into different categories and we treat those categories differently from one another
    - Integrated unicast and multicast
        - The multicast use the same topology
    - The hierarchical open shortest path first
        - We can divide up larger networks into hierarchies. So the broadcast is inside the partition **local area**. It does not care about the internal router in other **local area**. The **area border router** will connect into the **backbone** and it will connect to the other **backbone**. The **backbone** will connect to the **boundary router**. The **boundary router** is the gateway
            - The two hierarchy level is local area and backbone
            - The area border router is to connect to the backbone router
            - The backbone router is to connect to the other backbone router
            - The boundary router is to connect to the other autonomous system
        - The open shortest path firstis the link state protocol. The link state protocol is the router broadcast to the entire network. If it is the large network with a lot of routers, then there is a lot of broadcasts and it can be problematic. So we use the hierarchy and we do not overwhelm the network

9. The border gateway protocol
    - The BGP is the inter domain routing protocol
    - The BGP allows subsets to advertise the existence to the rest of the Internet
    - The eBGP obtain the subnet reachability info from the other autonomous system
    - The iBGP obtain the reachability info inside the autonomous system
    - The BGP session
        - the advertising path to the different destination network
        - The sessions in between autonomous systems use the **eBGP**. It is the semi permanent tcp connection between routers. It is not a technical thing. It is a policy consideration
        - The sessions inside the autonomous systems use the **iBGP**
    - The promise
        - If you set up the connection, you promise it will forward the datagram to the particular prefix
            - The **eBGP** is going to determine that which prefixes are going to be available via which autonomous systems and then it inform the particular autonomous system the new reachability information

10. The autonomous system aggregates all of the addresses it can get to and it shares one prefix via BGP

11. The BGP route
    - It is the prefix within irrelevant attributes

12. In BGP autonomous systems are defined by autonomous system numbers

13. The AS PATH
    - It contains the autonomous system numbers of the autonomous systems that an advertisement has gone through

14. The NEXT HOP indicate the internal AS router to the next hop AS

15. How does the entry get into the forwarding table
    - The process
        - The router is going to know how to reach the particular prefix
            - It is via BGP route advertisement from the other router 
        - The router determine the output port for the prefix
            - Use BGP route selection to find best inter AS route
            - Use OSPF to find best intra AS route leading to best inter AS route
            - Router identifies router port for that best route
        - The router enter prefix port combination into the forwarding table
    - The routers divided up into two layers. We have the routing plane and we have the forwarding plane
    - The routing table keeps track of a prefix
        - The prefix is the first part of that IP address and which port it needs to go through

16. The BGP contain the route. The route is the prefix and attribute and it contain the AS PATH and NEXT HOP. We are going to combine them

17. The router is going to receive multiple routes to the same prefix and the routers are going to exchange BGP information via **iBGP** route. It pick one route. The router select the route based on the shortest AS PATH

18. The router pick the best intra route
    - It use the NEXT HOP attribute
    - It use the open shortest path first, OSPF, to pick the shortest path

19. The tie in the shortest path
    - The hot potota routing
        - It suppose there is multiple best inter route, then pick the route with the closest NEXT HOP

20. The stub networks
    - It is that all traffic that enters a stub network is destined for a host in the network. All traffic that leave a stub network is originated in that network. It does not handle the other's traffic, it just handle its own traffic

21. The intra AS and the inter AS
    - The policy
        - There is policy in the inter AS. There is no policy in the intra AS
    - The scale
        - The hierarchical routing is much better at handling large scale networks
        - The hierarchical routing saves table size, reduced update traffic

22. The broadcast
    - The goal of broadcast routing is that we want to deliver packets from the source to all other hosts in the network

23. The source duplication
    - It is that the host create an individual packet for every host it send the packet to. It is going to individually address every individual packet to every individual host
    - It is inefficient
        - If you have thousands of hosts, then you are going to make the copy of every individual packet to send them to every single location

24. The in network duplication
    - It is not making an individual packet. It just forwards the same copy of the packets to every interface that it has
    - flooding
        - when node receives broadcast packet, sends copy to all neighbors
        - problems: cycles & broadcast storm
            - The cycles is that R1 broadcast to R2, the R2 repeat the broadcast to R3, and the R3 repeat the broadcast to R1. So it goes into the cycle
            - The broadcast storm is that we repeat the broadcast a lot of time. So we end up sending a lot of broadcast. So there is a lot of messages bouncing around the network and it just completely shut the network down
    - controlled flooding
        - node only broadcasts pkt if it hasn’t broadcast same packet before
        - node keeps track of packet ids already broadacsted
        - reverse path forwarding RPF
            - It determine what the minimum number of hops between you and the others initially pushed the packet. If you receive a packet that is not the least number of hops, then you just drop it
            - only forward packet if it arrived on shortest path between node and source
    - spanning tree
        - no redundant packets received by any node
        - The broadcast is inside the autonomous system. It exist inside the autonomous system

25. The multicasting
    - The goal is to find a tree connecting routers having local mcast group members








    




























































