## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1jXauDdm6QVfhAI-U_n8aGMINQ46H-fwA/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1sNyV39VzupZjenlM3Xqa9YLk1-D5wJ3f/preview" 
width="640" 
height="480"
allowfullscreen="allowfullscreen"></iframe>

## Quiz

1. Host and end system
    - The host and the end system are the same thing. We are sending data to
    - The end system is computer, workstation, and web server.      

2. Internet
    - Connecting a lot of devices

3. Communication link
    - Physical medium
        - fiber
        - copper
        - radio is wifi, 4G, and 5G

4. The bandwidth
    - The amount of data we can send to the link

5. The packet switch
    - forword packet

6. The standard
    - There is no standard. If the company own the device, they can do whatever they want to do.

7. The protocol
    - The sequence of steps.

8. The network edge
    - In system, the client and the server.

9. The network core
    - The cellphone tower.

10. The shared and the dedicated
    - The shared, wifi, cellphone tower
    - dedicated, it is not shared, DSL

11. LAN, short range. wide area, long range

12. The packet
    - The package of data.        

13. The twisted pair
    - Bundle together the wire pair.      

14. The guided media
    - The twisted-pair copper wire.    
    - The coaxial cable.      

15. The fiber cable
    - Low error rate, immune to the electromagnetic noise.
    - Travel long distance.

16. The satellite
    - It is slow. The radio speed is fast, but the distance is long.      


















