## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1sEHdw1Ka8u6lEaHszTGtG5atC7e4pSHG/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1rCn7TTHL-Zwly0uqy8530WdDfu5gQgBx/preview"></iframe>


## Lecture Video

<iframe 
src="https://drive.google.com/file/d/1w0iMzg2Qb-I5JglFYXcnfGenCZzpWZgG/preview" 
width="640" 
height="480"
allowfullscreen="allowfullscreen"></iframe>

## Quizz

1. The Internet is not originally designed to be secure. 
     
2. Internet protocal play catch up.
    - patch flaw

3. The DDoS
    - Multiple computer send a lot data to target computer.

4. The server client
    - server
        - it is always on server
        - permanent IP
        - data center
    - client
        - communicate with server
        - it is not permanently connected
        - dynamic IP
        - it is not directly communicate

5. The peer to peer
    - there is no always on server
    - it is directly communicate
    - self scalability, the new peer join the network, the network is bigger. they will bring new server and new compacity. the peer leave the network, the network is smaller. 
    - complex management

6. The socket is the function.      

7. The process on the host
    - IP address
    - port number

8. The data integrity
    - the file transfer

9. The audio will tolerate data loss.

10. The Internet phone, online game tolerate data loss, but require low delay.      

11. The multimedia requires minimum amount of throughput.      

12. The file transfer requires whatever throughput.      

13. The TCP
    - reliable.
    - flow control
        - The process maybe busy, the process maybe sleep, the process is not ready for it, the process receives the data, it does not mean that the process consumes the data. The data is received, but is maybe lost. The TCP will not allow this happen. TCP will ensure that the sender doesn’t send more data than the receiver can buffer.      
        - The TCP will ensure that the sender doesn’t send more data than the receiver can buffer.      
    - there is no guarante timing and throughput.       

14. The UDP
    - there is not a lot data
    - the faster transfer speed



    






