## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1qglL4SpEFlCELjA4b52U22LGt1JaQu03/preview"></iframe>

## Lecture Video

<iframe 
src="https://drive.google.com/file/d/1VyigAu9bKT-tV7qUgA2LRD3V1PiNmGtY/preview" 
width="640" 
height="480"
allowfullscreen="allowfullscreen"></iframe>

## Quiz        

1. The mesh of router
    - Connecting to multiple router

2. The packet switching
    - Pick the path to send data

3. The store and forward
    - The entire packet got to arrive before we can make the switching decision.      

4. The queue delay loss
    - arrival rate is greater than transmission rate
        - packet will go to queue and wait
        - if memory fills up, packet will be dropped

5. Internet structure
    - The ISP connecting to every other ISP
    - The ISP connecting to the global ISP
    - The ISP connecting to large center ISP and use peering link to connect the large ISP

6. The throughput
    - capacity of link

7. The Internet protocal stack
    - application, program
    - transport, TCP, UDP, what data to send, when to send the data
    - network, one side of Internet to another side of Internet
    - link, 4G, 5G, it controls how we communicate with the medium, cable to cable









