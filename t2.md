## Quiz      

1. What is a network partition?
    - It is the failure of a part of the network. The fault tolerance

2. True or False: Datagram / packet-switched networks are connectionless services.
    - True

3. Why is the data forwarding plane implemented in hardware?
    - Speed

4. True or False: Virtual circuit networks are connectionless services.
    - False

5. Why can’t the Internet use a “flag day” to completely transition to IPv6?
    - The internet is too large 
    - There is too much coordination would be required 
    - There is no central authority

6. Where does most packet loss occur on a router, and why?
    - It occur at the output port and the input port 
    - The first is the output port buffer overflow. It is that the switching fabric is faster than the medium as well as there is the situation where multiple input port can be empty into the same output port
    - The second is the input port buffer overflow. It is that the fabric slower than input port

7. True or False: Datagram / packet-switched networks explicitly notify hosts when data are lost.
    - False

8. Why might network administrators elect to use a static configuration for hosts on their networks?
    - If they want the route to change slowly over time 
    - If they want to configure the route manually

9. Briefly explain the purpose of NAT.
    - It converts between internal IP addresses and the external address used by the Internet

10. Briefly explain the concept of longest prefix matching.
    - It is the algorithm to find the entry in the forwarding table. It is that when looking for the forwarding table, use the longest prefix matching to match the destination address

11. Briefly explain the difference between forwarding and routing.
    - The forwarding is that it physically move the packet from one port to another port on the same router
    - The routing is that it determine the best path through the network

12. Why can we not implement a scheduling discipline for selectively forwarding datagrams on routers in the United States?
    - The policy
    - The network neutrality

13. What was the initial motivation of IPv6?
    - It provide more TCP/IP address

14. In packet-switched networks, is every datagram guaranteed to take the same path through the network?
    - No

15. How many IP addresses does a router have?
    - One for each port

16. Why were fields removed from the IPv6 header?
    - In order to make forwarding faster

17. Briefly explain the concept of a “broadcast domain.”  On a wired LAN, what piece of networking equipment frequently breaks up a broadcast domain?
    - It is that one to all transmission of data. Anything broadcast will be received by everyone in the network
    - The router break up the broadcast domain

18. In most circumstances, how does a host know what its default gateway is
    - DHCP

19. Why does the IPv4 header include a length for both the header and the overall length of the datagram?
    - The header can have a variable length and the payload can have a variable length

20. Why can packet-switched networks not offer quality of service minimums?
    - Because the route packets take in a packet switched network is not fixed, and resources are not allocated along each hop


1. What is the main difference between a switch and a router?
    - The router use the network layer header. It use the routing algorithm to compute the forwarding table
    - The switch use the link layer header. It is the self assemble the forwarding table

2. MPLS borrows a number of ideas from what networking paradigm?
    - The virtual circuit
    
3. In CSMA, if nodes are checking the medium before broadcasting in order to avoid transmitting over another node, how do collisions still occur?
    - The problem is that there is propagation delay. So none of the signal go out to the entire medium at the same time

4. In the context of Content Distribution Networks, briefly describe the “enter deep” server position strategy.
    - Push CDN server deep into many access network. So put server cluster in a lot of locations. So the user is closed to the location

5. In the context of DASH, describe one corrective action a client may take if its connection to the current CDN server degrades?
    - It consult manifest to switch the URL and request the lower quality chunk

6. MPLS borrows a number of ideas from what networking paradigm?
    - The virtual circuit

7. In the context of Content Distribution Networks, briefly describe the “bring home” server position strategy
    - Build up the small number of large servers near a point-of-presence

8. Briefly describe the concept of a VLAN.
    - We can set up the virtual local area network for group of people. We can partition the network. So it is like there are different pieces of network equipment. We can have the control. So we can dynamically assign port

9. Why might it be desirable to use UDP to stream multimedia?  What is a common complication with using UDP to stream multimedia?
    - There is no flow control and no congestion control. We can send data as fast as it can as well as we can send data as much as it can
    - The problem is the error recovery. The problem is that the UDP cannot go through firewalls. The problem is that it require the media control server. The play out is not continuous

10. Give a brief definition of “network jitter”.
    - It is the difference in latency between one time and another time. There is the fluctuation. The time between packet generate and receive is fluctuate

11. In TDMA, what happens to a time slot if a node has nothing to send or receive during its slot?
    - It goes unused and goes idle

12. What changes do hosts need to make when a switch is added to a network?
    - It does not do anything

13. What is the difference between “network loss” and “delay loss”?
    - The network loss is the packet lost in network 
    - The delay loss is the packet arrived correctly, but it is not arriving fast enough

14. Why is “source duplication” inefficient?
    - You have to make the copy of the same packet for each system. So there is overhead on the source and overhead link the source

15. In the context of streaming multimedia, what is the downside to streaming higher quality media?
    - It require more throughput

16. How does a machine know its MAC address?
    - It is burned onto ROM chip of NIC

17. Why was collision detection added to CSMA?
    - To abort transmissions that collided 
    - To reduce the amount of time wasted during collisions
    - To increase the efficiency of the CSMA

18. In the context of the data link layer, what is meant by the terminology “star topology”?
    - There is central device and everyone connect to it

19. There are generally three ways for multiple nodes to share the same medium.  Name them and give a brief description.
    - The channel partitioning. It is to divide the channel up. It assign the channel to a node. It is exclusive to you
    - The random access. There is no divide. Everyone can have full access to the medium
    - The taking turn. Everyone have to take turn to get opportunity to transmit

20. There are generally three application types of streaming media.  Name them and give a brief definition of them.
    - The streaming stored. We download the file as well as encode it. We play it before we downloaded the entire file
    - The conversational. It is like the real time dialogue. It is the people talking back and forth. There is stream of data go to both direction
    - The streaming live. It is the broadcast as it happens


1. The VoIP end-end-delay requirement
    - The higher delays is noticeable, impair interactivity

2. The spurts
    - speaker’s audio: alternating talk spurts, **silent periods**
    - When people have conversation, they stop talk and start listen. They are going to be silent and they are going to start listen

3. The network loss and the delay loss
    - The network loss is that the packet lost
    - The delay loss is that the packet is late for playout. We do not receive the packet fast enough. The packet go into the queue and wait

4. The receiver attempt to play out the chunk at the fixed delay
    - If the delay is fluctuating, the human is going to notice it

5. It is not playing it back milliseconds after it was received. It is playing it back milliseconds after it was generated
    - The generated is not the received
    - The tradeoff
        - The smaller delay the better interactive experience
        - The larger delay the less packet loss

6. We would like to keep that **same** playout **delay** because our brain can patch that little space and we do not notice it. If the **delay** **fluctuates**, we are going to notice it

7. VoiP: recovery from packet loss
    - Challenge: recover from packet loss given small tolerable delay between original transmission and playout
        - each ACK/NAK takes ~ one RTT
        - alternative: Forward Error Correction (FEC)
            - Instead of attempting to retransmit the packet, we reconstruct what we lost
            - There is no retransmission. We send enough redundant bits. If there is packet lost, we can use the redundant bits to reconstruct the packet
            - We send enough bits to allow recovery without retransmission

8. Voice-over-IP: Skype
    - proprietary application-layer protocol (inferred via reverse engineering)
        - encrypted msgs
    - P2P components
    - clients: skype peers connect directly to each other for VoIP call
    - super nodes (SN): skype peers with special functions
    - overlay network: among SNs to locate SCs
    - login server

9. The skype client operation
    - joins skype network by contacting SN (IP address cached) using TCP
    - logs-in (usename, password) to centralized skype login server
    - obtains IP address for callee from SN, SN overlay
        - or client buddy list
    - initiate call directly to callee

10. Skype: peers as relays
    - problem: both Alice, Bob are behind “NATs” 
        - NAT prevents outside peer from initiating connection to insider peer 
        - inside peer can initiate connection to outside
    - relay solution: Alice, Bob maintain open connection to their SNs 
     - Alice signals her SN to connect to Bob 
     - Alice’s SN connects to Bob’s SN 
     - Bob’s SN connects to Bob over open connection Bob initially initiated to his SN
     - P2P techniques are also used in Skype relays, which are useful for establish- ing calls between hosts in home networks

11. SIP: Session Initiation Protocol
    - all telephone calls, video conference calls take place over Internet
    - people identified by names or e-mail addresses, rather than by phone numbers
    - can reach callee (if callee so desires), no matter where callee roams, no matter what IP device callee is currently using

12. SIP provides mechanisms for call setup
    - for caller to let callee know she wants to establish a call
    - so caller, callee can agree on media type, encoding
    - to end call

13. The protocol agnostic
    - It is not tied to one specific protocol

14. The setting up call to known IP address
    - Alice’s SIP invite message indicates her port number, IP address, encoding she prefers to receive (PCM mlaw)
        - How Alice know Bob IP address
            - There is intermediate SIPservers. when Bob starts SIP client, client sends SIP REGISTER message to the registrar server
    - Bob’s 200 OK message indicates his port number, IP address, preferred encoding (GSM) 
    - SIP messages can be sent over TCP or UDP; here sent over RTP/UDP 
    - default SIP port number is 5060

15. The SIP proxy
    - another function of SIP server: proxy 
    - Alice sends invite message to her proxy server 
        - contains address sip:bob@domain.com 
        - proxy responsible for routing SIP messages to callee, possibly through multiple proxies 
    - Bob sends response back through same set of SIP proxies 
    - proxy returns Bob’s SIP response message to Alice 
        - contains Bob’s IP address 
    - SIP proxy analogous to local DNS server plus TCP setup


