## Quiz        

1. What is a network partition?
    - It is the failure of a part of the network. The fault tolerance

2. True or False: Datagram / packet-switched networks are connectionless services.
    - True

3. Why is the data forwarding plane implemented in hardware?
    - Speed

4. True or False: Virtual circuit networks are connectionless services.
    - False

5. Why can’t the Internet use a “flag day” to completely transition to IPv6?
    - The internet is too large 
    - There is too much coordination would be required 
    - There is no central authority

6. Where does most packet loss occur on a router, and why?
    - It occur at the output port and the input port 
    - The first is the output port buffer overflow. It is that the switching fabric is faster than the medium as well as there is the situation where multiple input port can be empty into the same output port
    - The second is the input port buffer overflow. It is that the fabric slower than input port

7. True or False: Datagram / packet-switched networks explicitly notify hosts when data are lost.
    - False

8. Why might network administrators elect to use a static configuration for hosts on their networks?
    - If they want the route to change slowly over time 
    - If they want to configure the route manually

9. Briefly explain the purpose of NAT.
    - It converts between internal IP addresses and the external address used by the Internet

10. Briefly explain the concept of longest prefix matching.
    - It is the algorithm to find the entry in the forwarding table. It is that when looking for the forwarding table, use the longest prefix matching to match the destination address

11. Briefly explain the difference between forwarding and routing.
    - The forwarding is that it physically move the packet from one port to another port on the same router
    - The routing is that it determine the best path through the network

12. Why can we not implement a scheduling discipline for selectively forwarding datagrams on routers in the United States?
    - The policy
    - The network neutrality

13. What was the initial motivation of IPv6?
    - It provide more TCP/IP address

14. In packet-switched networks, is every datagram guaranteed to take the same path through the network?
    - No

15. How many IP addresses does a router have?
    - One for each port

16. Why were fields removed from the IPv6 header?
    - In order to make forwarding faster

17. Briefly explain the concept of a “broadcast domain.”  On a wired LAN, what piece of networking equipment frequently breaks up a broadcast domain?
    - It is that one to all transmission of data. Anything broadcast will be received by everyone in the network
    - The router break up the broadcast domain

18. In most circumstances, how does a host know what its default gateway is
    - DHCP

19. Why does the IPv4 header include a length for both the header and the overall length of the datagram?
    - The header can have a variable length and the payload can have a variable length

20. Why can packet-switched networks not offer quality of service minimums?
    - Because the route packets take in a packet switched network is not fixed, and resources are not allocated along each hop


























