## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1tfZyWsjbBWYeka7vh11JPPK_R2yl_vSl/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1wL_1ABXFkkLHGge4fliJqsp3yDNzp9pP/preview" width="640" height="480"></iframe>

<iframe src="https://drive.google.com/file/d/1pYpmkLdmsgdeMdJjXTrIgLnSI1F6dCSD/preview" width="640" height="480"></iframe>

## Quiz      

1. The web and http is object
    - The object is html, jpeg image, java, and audio

2. The http use tcp and stateless
    - It does not store the info regarding what the client has done if the client get disconnected.        
    - The reason is that it make developer's job easier. It is diffcult to maintain the state.        

3. The http connection
    - The non persistent, one object sent to TCP
    - The persistent, multiple object sent to TCP

4. The http request
    - We put string together with ASCII

5. The uploading
    - The post, the input is uploaded to server
    - The URL, the input is uploaded in URL

6. The Telnet

7. The GET

8. The cookie        
    - It is the text file that is createed and stored on the local system.  
    - What the cookie do is that they are a way of giving web server appearance as if it is maintaining states in between sessions.
    - The privacy.        

9. The web cache
    - It is the copy of data.      
    - It reduce response time for client request.      
    - It reduce load request for origin server.     

10. The conditional GET
    - The 304, not modified.
    - The 200, the proxy server does not have the data.

11. The FTP
    - The TCP control connection and the TCP data connection. It is the out of band.      

12. The email
    - The user agent.
    - The mail server. The mailbox is on the mail server.        
    - The simple mail transfer.

13. The pop 3 email
    - It just download.      



    






    












