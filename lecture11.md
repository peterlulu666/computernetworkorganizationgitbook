## Quiz        

1. The network layer is sandwiched between the transport layer and the link layer.       

2. The IP is the only protocol carry our traffic.      

3. The length field specify the length of the entire packet.      

4. We do not use type of service.      

5. The TTL time to live
    - It is decremented at each router.      

6. The upper layer        
    - It specifies which upper layer protocol you are going to be delivered to. It is either TCP or UDP.        
7. The overhead
    - The 20 byte TCP
    - The 20 byte IP

8. The fragmentation
    - The MTU is at the data link layer.        
    - The MTU is the maximum transmission unit.        
    - It is vary.      
    - There is one datagram, we divide it to the smaller datagram, and reassemble them at the destination.        

9. The IP header use bit to identify the fragment.        

10. How many IP addresses does a router have?
    - One for each port
    - There is one for every single interface.       

11. The subnet is the self contained network. It is the isolated network.         

12. We designate the subnet based on parts of the IP address.      
    - If you are on the same subnet, it is assumed that all the system on the same subnet can talk back and forth to one another without having to go through the router.      
    - It can physically reach other router without intervening router.        

13. The subnet contains broadcasts.      

14. The slash 24      
    - It is the 1st 24 bits of this address is dedicated to the network.      


15. The CIDR      
    - Classless Inter Domain Routing

16. How to receive IP address
    - DHCP
    - The Dynamic Host Configuration Protocol
    - obtain IP address
        - renew lease
        - reuse address
        - mobile join the network
    - broadcast

17. ISP block address
    - ICANN

18. NAT
    - network address translation.      
    - The entire network see our local network as the single individule IP address.      
        - We do not need range of IP address for every device
        - We can change IP address in the local network and we do not have to tell others
        - If we change ISP we do not have to change the local network address
        - It is not visible by others
    - problem
        - routers should only process up to layer 3
        - use the P2P

19. ICMP
    - Internet Control Message Protocol
    - destination host unreachable
    - destination port unreachable

20. IPv6
    - There is no checksum
    - The option is outside of header
    - ICMPv6
    - There is no fragmentation

21. The tunneling
    - We can put IPv4 header to the beginning of the IPv6 datagram. We will treat the entire thing as the IPv4 datagram.      



































