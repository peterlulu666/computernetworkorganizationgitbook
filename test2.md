## Quiz        

1. Why can packet-switched networks not offer quality of service minimums?
    - Because the route packets take in a packet switched network is not fixed, and resources are not allocated along each hop

2. Briefly explain the purpose of NAT.
    - It convert between internal IP address and external address

3. List the Quality of Service (QoS) minimums provided by packet-switched networks.
    - There aren’t any

4. What is the difference between “end system-to-end system” communication and “host-to-host” communication?
    - “End system-to-end system” are just the two clients communicating 
    - “Host-to-host” is every hop in the network

5. Why can we not implement a scheduling discipline for selectively forwarding datagrams on routers in the United States?
    - The policy 
    - The network neutrality

6. What is a network partition?
    - When a network is divided due to an equipment failure

7. What is the purpose of IP fragmentation?
    - We divide datagram into smaller datagram and reassemble them at the destination

8. Why could virtual circuit networks offer quality of service minimums?
    - Because they configured an end-to-end path through the entire network, and allocated resources at each hop prior to sending/receiving data

9. Why is the data forwarding plane implemented in hardware?
    - Speed

10. Why might network administrators elect to use a static configuration for hosts on their networks?
    - Security

11. What is the difference between “multicasting” and “broadcasting”?
    - The multicasting is the one to many data transmission. It is one sender to multiple receiver
    - The broadcasting is the one to all data transmission. It is the one sender to every receiver

12. What is the main difference between a switch and a router?
    - The switch use link layer header. It is self assemble forwarding table
    - The router use network layer header. It use routing algorithm to compute forwarding table

13. In the context of Content Distribution Networks, briefly describe the “bring home” server position strategy.
    - Build up the small number of large server near point of presence

14. What is the difference between “network loss” and “delay loss”?
    - The network loss is the packet lost in network
    - The delay loss is the packet arrived correctly, but it is not arriving fast enough

15. Why was collision detection added to CSMA?
    - To abort transmission that collided
    - To reduce time wasted during collision
    - To increase efficiency

16. In the context of the data link layer, what is meant by the terminology “star topology”?
    - There is central device and every one connecting to it

17. In token-passing protocols, what happens if a node has nothing to transmit when it receives the token?
    - You forward the token to the next sequentially

18. How does a machine know its MAC address?
    - It is burned onto ROM chip of NIC

19. In TDMA, what happens to a time slot if a node has nothing to send or receive during its slot?
    - It goes unused and goes idle

20. In the context of random access protocols, what is meant by a “collision”?  Is a collision indicative of a malfunction?
    - We have two or more simultaneous transmission. The transmission is at the same time. They run into each other. So everything you just transmitted is garbled
    - No

21. Why does VoIP have a hard upper bound on delay?
    - If you use forward error correction with UPD. The UPD is not reliable. So there is hard upper bound on delay, if we drop something with UDP, it is possible to rebuild it with forward error correction

22. Why might retransmitting a lost packet not be an acceptable solution to network loss when using conversational multimedia?
    - Because retransmitting takes time and conversational multimedia has a hard upper bound on when it has to playback a packet

23. In some cases, Skype will use UDP.  Given that UDP is a best-effort service, how then does Skype recover from network loss?
    - It use forward error correction. So there is no retransmission. We send enough redundant bit. We use it to reconstruct the lost packet

24. In the context of VoIP, what is the difference between the time a packet was generated and the time the packet was received?
    - Generated time is actually created by the remote machine; received time is the time it took for the packet to transit the network and be received by the local machine

25. What is a 'mnemonic' in the context of SIP?
    - A name that’s easy for humans to remember















