## Quiz      

1. Question 1
    - 5 / 5 pts
    - What is the organization responsible for enforcing standards on the Internet?
    - Your Answer:
    - There is no standards. If the company own the device, they can do whatever they want to do.      

1. Question 2
    - 5 / 5 pts
    - Why is simply having the IP address not enough to send data to the correct destination process?
    - Your Answer:
    - The system will have multiple processes with active TCP connections running on it at the same time. It requires port number to identify the correct process.         

1. Question 3
    - 5 / 5 pts
    - Give an example of an app that might require low delay.
    - Your Answer:
    - The online game.      

1. Question 4
    - 5 / 5 pts
    - Give an example of an app that might not require 100% data integrity.
    - Your Answer:
    - The Internet phone.        

1. Question 5
    - 4 / 5 pts
    - In an enterprise network, why do servers and switches almost always connect to the network with wired connections?
    - Your Answer:
    - The enterprise network will use the dedicated bandwidth, but not the shared.      

1. Question 6
    - 5 / 5 pts
    - What does the ‘traceroute’ or ‘tracert’ program do?
    - Your Answer:
    - It provides the delay measurement.     

1. Question 7
    - 5 / 5 pts
    - There are many types of radio links.  Name two of them.
    - Your Answer:
    - The wifi and 4G.        

1. Question 8
    - 5 / 5 pts
    - What is meant by calling network layers “modular”?
    - Your Answer:
    - They have different function and purpose.     
    - It is self-contained and that the layers are independent of one another.      

1. Question 9
    - 5 / 5 pts
    - What information is used by a process running on one host to identify a process running on another host? 
    - Your Answer:
    - The IP address
    - The port number

1. Question 10
    - 5 / 5 pts
    - Give an example of an app that might require high throughput.
    - Your Answer:
    - The online game.     
    - The video call.      

1. Question 11
    - 5 / 5 pts
    - Which source of packet delay produces the most variation in latency?
    - Your Answer:
    - The queue delay.     

1. Question 12
    - 3 / 5 pts
    - In the context of electrical engineering, what is the difference between “bandwidth” and “throughput”?
    - Your Answer:
    - The bandwidth is the amount of data we can send to the link.       
    - The throughput is the capacity of the link.    
    - The bandwidth determines how many packets can be sent and the throughput determines how many packets it will transmit.     

1. Question 13
    - 5 / 5 pts
    - Why is fiber optic cable immune to electromagnetic interference?
    - Your Answer:
    - It is the light in glass, so it can immune to electromagnetic interference.        

1. Question 14
    - 5 / 5 pts
    - What are the congestion implications with regards to a mesh of routers?
    - Your Answer:
    - It will not transmit packet at full link capacity.   
    - In a mesh, each router is receiving data from multiple sources, which compounds congestion.           

1. Question 15
    - 5 / 5 pts
    - Why do ISPs not have a direct connection with every other ISP?
    - Your Answer:
    - It does not scale.      

1. Question 16
    - 5 / 5 pts
    - Briefly describe what is meant by a “content provider network”.
    - Your Answer:
    - It uses the private network to connect the large data center to internet and bring service close to the user.        

1. Question 17
    - 5 / 5 pts
    - In the context of circuit switching, what happens to your allocated channel resources if you’re not using them?
    - Your Answer:
    - It will be idle. It will not be efficient.        

1. Question 18
    - 5 / 5 pts
    - With regards to designing protocols, what is one benefit of assuming the server is always on?
    - Your Answer:
    - It is the convenient assumption.      
    - We know what the first step in the protocol is going to be. The server is there listening for connection. This makes the architecture very easy to develop.        

1. Question 19
    - 5 / 5 pts
    - List the five layers of the TCP/IP protocol stack.
    - Your Answer:
    - application layer
    - transport layer
    - network layer
    - link layer
    - physical layer

1. Question 20
    - 5 / 5 pts
    - What is the “package” of data at each layer of the TCP/IP protocol stack called?
    - The transport layer, layer 4 is called the segment.
    - The network layer, layer 3 is called the packet.
    - The data link layer, layer 2 is called the frames.

