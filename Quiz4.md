## Quiz      

1. What is the main difference between a switch and a router?
    - The router use the network layer header. It use the routing algorithm to compute the forwarding table
    - The switch use the link layer header. It is the self assemble the forwarding table

2. MPLS borrows a number of ideas from what networking paradigm?
    - The virtual circuit
    
3. In CSMA, if nodes are checking the medium before broadcasting in order to avoid transmitting over another node, how do collisions still occur?
    - The problem is that there is propagation delay. So none of the signal go out to the entire medium at the same time

4. In the context of Content Distribution Networks, briefly describe the “enter deep” server position strategy.
    - Push CDN server deep into many access network. So put server cluster in a lot of locations. So the user is closed to the location

5. In the context of DASH, describe one corrective action a client may take if its connection to the current CDN server degrades?
    - It consult manifest to switch the URL and request the lower quality chunk

6. MPLS borrows a number of ideas from what networking paradigm?
    - The virtual circuit

7. In the context of Content Distribution Networks, briefly describe the “bring home” server position strategy
    - Build up the small number of large servers near a point-of-presence

8. Briefly describe the concept of a VLAN.
    - We can set up the virtual local area network for group of people. We can partition the network. So it is like there are different pieces of network equipment. We can have the control. So we can dynamically assign port

9. Why might it be desirable to use UDP to stream multimedia?  What is a common complication with using UDP to stream multimedia?
    - There is no flow control and no congestion control. We can send data as fast as it can as well as we can send data as much as it can
    - The problem is the error recovery. The problem is that the UDP cannot go through firewalls. The problem is that it require the media control server

10. Give a brief definition of “network jitter”.
    - It is the difference in latency between one time and another time. There is the fluctuation. The time between packet generate and receive is fluctuate

11. In TDMA, what happens to a time slot if a node has nothing to send or receive during its slot?
    - It goes unused and goes idle

12. What changes do hosts need to make when a switch is added to a network?
    - It does not do anything

13. What is the difference between “network loss” and “delay loss”?
    - The network loss is the packet lost in network 
    - The delay loss is the packet arrived correctly, but it is not arriving fast enough

14. Why is “source duplication” inefficient?
    - You have to make the copy of the same packet for each system. So there is overhead on the source and overhead link the source

15. In the context of streaming multimedia, what is the downside to streaming higher quality media?
    - It require more throughput

16. How does a machine know its MAC address?
    - It is burned onto ROM chip of NIC

17. Why was collision detection added to CSMA?
    - To abort transmissions that collided 
    - To reduce the amount of time wasted during collisions
    - To increase the efficiency of the CSMA

18. In the context of the data link layer, what is meant by the terminology “star topology”?
    - There is central device and everyone connect to it

19. There are generally three ways for multiple nodes to share the same medium.  Name them and give a brief description.
    - The channel partitioning. It is to divide the channel up. It assign the channel to a node. It is exclusive to you
    - The random access. There is no divide. Everyone can have full access to the medium
    - The taking turn. Everyone have to take turn to get opportunity to transmit

20. There are generally three application types of streaming media.  Name them and give a brief definition of them.
    - The streaming stored. We download the file as well as encode it. We play it before we downloaded the entire file
    - The conversational. It is like the real time dialogue. It is the people talking back and forth. There is stream of data go to both direction
    - The streaming live. It is the broadcast as it happens








