## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1zCYp9kANE1MU0xJfd_aapyOP4VNWoouE/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1eAR3FM9eJZ0WRDrxWE8wJCKAh3VSpOtb/preview" width="640" height="480"></iframe>

<iframe src="https://drive.google.com/file/d/1rs2WwckZQNi3jtMxgghkDSF0UuyKSY77/preview" width="640" height="480"></iframe>

## Quiz 

1. There is no always on server      
    - The first step is not clear.          
    - The arbitrary end system has constraint.      
    - The peer is intermittently connected and change the IP address.      

2. The BitTorrent
    - The torrent is the client.        
    - The tracker is the server.        

3. The top four peer how to get the first chunk
    - After 30 second, the existing peer will pick someone at random. If they select Alice, they will optimistically unchoke Alice, so Alice will join it and get the first chunk.      

4. The transport layer
    - The logical communication between process.      
    - It runs on the in system.      

5. The transport protocol used on the router
    - There is no one.      

6. The network layer and the transport layer
    - The network layer is the logical communication between host.
        - The host is everything that you can send data to.
    - The transport layer is the logical communication between process. The application talk to the application. It ensure that the process and the application will have the data. It runs on the in system.        

7. The congestion control
    - It ensure that it does not overwhelm the buffer space.      

8. The flow control
    - It ensure that it does not overwhelm the receiver.      
    - The TCP will ensure that the sender doesn’t send more data than the receiver can buffer.      

9. The UDP is the best effort
    - It the the one effort.
    - It will try one time.        

10. There is no delay guarantee and bandwidth guarantee.      

11. The TCP and the UDP are important protocol and there is more protocol.        






 





















