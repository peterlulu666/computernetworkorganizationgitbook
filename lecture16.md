## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1TauCRDSVan4ANe-HzdtE3ax9Bjmr83d6/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/13dSZQtYUuqzfEl7sTGMxeyMoCtKv2HoB/preview" width="640" height="480"></iframe>

## Quiz      

1. The link virtualization
    - Multiprotocol Label Switching MPLS
        - The initial goal: high-speed IP forwarding using fixed length label
            - The fast lookup using fixed length identifier
            - It borrowing ideas from Virtual Circuit (VC) approach
            - The IP datagram still keeps IP address

2. The MPLS capable routers
    - The MPLS routers is the label switch routers
        - The forwarding decision is only based on the label
    - The flexibility: MPLS forwarding decisions can differ from those of IP
        - use destination and source addresses to route flows to same destination differently 
        - re-route flows quickly if link fails: pre-computed backup paths
    - The traffic engineering
        - The capabilities
        - It allow us to have multiple paths through the network. we can figure out those paths ahead of time. We can move traffic to different places based on the purpose of that traffic or the ultimate destination of that traffic

3. MPLS versus IP paths
    - The IP routing: path to destination determined by **destination address** alone
    - The MPLS routing: path to destination can be based on **source and destination address**
        - fast reroute: precompute backup routes in case of link failure

4. If it is not based for that data center, it route in a different direction. So this could reduce congestion on this link and allow traffic that is not distant for that data center to move a little less encumbered through the network. If you are going to have a link failure, it can adjust to link failures instantaneously

5. Data center networks
    - The 10’s to 100’s of thousands of hosts, often closely coupled, in close proximity:
    - The challenges
        - The multiple applications, each serving massive numbers of clients
        - We spread it around first of all, to distribute the workload, and then we also want to avoid bottlenecks

6. The rack
    - The inside each one of these racks is a **blade** 
        - The **balde** is the self contained computer has its own power supply
        - There is numerous virtual machines that are running inside the **blades**
    - The TOR switches
        - It is the switch on top of rack
        - The switch on top of the rack is there to help us to move data in between each one of these physical blades and we can isolate the data
    - The self contained help us with the **internal traffic**
        - The **internal traffic** is parts of the data center and it to other parts of the data center
    - The external traffic
        - It is that bring data into the data center

7. The data centers connect to the outside world with one or more **border routers**

8. If the user receive the data, it go back up to the **load balancer**. The **load balancer** is going to do your address translation and push it to the data center

9. The rich interconnection among switches, racks:
    - It increased throughput between racks
    - It increased reliability via redundancy






    
        
    


























