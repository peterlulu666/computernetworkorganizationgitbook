## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/137z4WAl3Ch9LeXFy9rnqA4-50hth6WKG/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1a9k4HRuJeugUxfIkK3Adznhna-FK4UPT/preview" width="640" height="480"></iframe>

## Quiz      

1. The VoIP end-end-delay requirement
    - The higher delays is noticeable, impair interactivity

2. The spurts
    - speaker’s audio: alternating talk spurts, **silent periods**
    - When people have conversation, they stop talk and start listen. They are going to be silent and they are going to start listen

3. The network loss and the delay loss
    - The network loss is that the packet lost
    - The delay loss is that the packet is late for playout. We do not receive the packet fast enough. The packet go into the queue and wait

4. The receiver attempt to play out the chunk at the fixed delay
    - If the delay is fluctuating, the human is going to notice it

5. It is not playing it back milliseconds after it was received. It is playing it back milliseconds after it was generated
    - The generated is not the received
    - The tradeoff
        - The smaller delay the better interactive experience
        - The larger delay the less packet loss

6. We would like to keep that **same** playout **delay** because our brain can patch that little space and we do not notice it. If the **delay** **fluctuates**, we are going to notice it

7. VoiP: recovery from packet loss
    - Challenge: recover from packet loss given small tolerable delay between original transmission and playout
        - each ACK/NAK takes ~ one RTT
        - alternative: Forward Error Correction (FEC)
            - Instead of attempting to retransmit the packet, we reconstruct what we lost
            - There is no retransmission. We send enough redundant bits. If there is packet lost, we can use the redundant bits to reconstruct the packet
            - We send enough bits to allow recovery without retransmission

8. Voice-over-IP: Skype
    - proprietary application-layer protocol (inferred via reverse engineering)
        - encrypted msgs
    - P2P components
    - clients: skype peers connect directly to each other for VoIP call
    - super nodes (SN): skype peers with special functions
    - overlay network: among SNs to locate SCs
    - login server

9. The skype client operation
    - joins skype network by contacting SN (IP address cached) using TCP
    - logs-in (usename, password) to centralized skype login server
    - obtains IP address for callee from SN, SN overlay
        - or client buddy list
    - initiate call directly to callee

10. Skype: peers as relays
    - problem: both Alice, Bob are behind “NATs” 
        - NAT prevents outside peer from initiating connection to insider peer 
        - inside peer can initiate connection to outside
    - relay solution: Alice, Bob maintain open connection to their SNs 
     - Alice signals her SN to connect to Bob 
     - Alice’s SN connects to Bob’s SN 
     - Bob’s SN connects to Bob over open connection Bob initially initiated to his SN
     - P2P techniques are also used in Skype relays, which are useful for establish- ing calls between hosts in home networks

11. SIP: Session Initiation Protocol
    - all telephone calls, video conference calls take place over Internet
    - people identified by names or e-mail addresses, rather than by phone numbers
    - can reach callee (if callee so desires), no matter where callee roams, no matter what IP device callee is currently using

12. SIP provides mechanisms for call setup
    - for caller to let callee know she wants to establish a call
    - so caller, callee can agree on media type, encoding
    - to end call

13. The protocol agnostic
    - It is not tied to one specific protocol

14. The setting up call to known IP address
    - Alice’s SIP invite message indicates her port number, IP address, encoding she prefers to receive (PCM mlaw)
        - How Alice know Bob IP address
            - There is intermediate SIPservers. when Bob starts SIP client, client sends SIP REGISTER message to the registrar server
    - Bob’s 200 OK message indicates his port number, IP address, preferred encoding (GSM) 
    - SIP messages can be sent over TCP or UDP; here sent over RTP/UDP 
    - default SIP port number is 5060

15. The SIP proxy
    - another function of SIP server: proxy 
    - Alice sends invite message to her proxy server 
        - contains address sip:bob@domain.com 
        - proxy responsible for routing SIP messages to callee, possibly through multiple proxies 
    - Bob sends response back through same set of SIP proxies 
    - proxy returns Bob’s SIP response message to Alice 
        - contains Bob’s IP address 
    - SIP proxy analogous to local DNS server plus TCP setup


