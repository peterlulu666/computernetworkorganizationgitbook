## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1WMUMEfTLyU0EOLhfUzF-rrvtY0yegY99/preview"></iframe>

## Lecture Video



<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1ioanCkJSPHNSH_2TYROdOjxUiK2C0A4b/preview"></iframe>

## Quiz        

1. The link layer
    - The **link layer** is responsible for moving data between **physically adjacent** nodes
        - The **physically adjacent** nodes is that 2 nodes connected by a medium 
    - The **transport layer** is about in system to end system
        - Move info between phone and server
    - The **network layer** is about routing and forwarding
        - How to push data
        - The least cost path between any two in systems
        - Where is the data going to travel. It is routing

2. The wireless link is still a **medium** and it is still a **physical process**

3. the layer 2 packet is called frame. The layer 4 packet is called segment. The layer 3 is packet

4. The reliability mechanisms built into the medium
    - Yes.
    - The medium recognize it and correct for it

5. The fiber optic is immune to electromagnetic interference
    - It is glass

6. The trailer
    - It is to put the data at the end of the package in order to tell the controller to stop listen for signals

6. The Mac address
    - It is at the link layer
    - It is different from your IP address
    - It is used to **identify** the source and the destination
    - The medium is shared using link layer services. So we have exclusive access to a medium
    - We share it with a lot of medium. We share it with a lot of node. The signal can be received by a lot of node at the same time. We use the MAC address to **identify** the node

7. The error detection and the error correction built into the link layer
    - The end to end reliability
        - The **error detection** built into TCP and UDP
    - The IPv4
        - The **error detection** built into it
    - The link layer
        - The **error detection** built into it
        - The application does not have to worry about getting the wrong data
        - It is not to overwhelm the receiving node. It is not to overwhelm the medium
        - The **link layer flow control** is to ensure not to overwhelm the medium between the sender and the receiver

8. The half duplex
    - It is that either send data or receive data. It cannot do both at the same time

9. The full duplex
    - It is that both send data and receive data at the same time

10. The link layer implemented at the NIC network interface card
    - Nowadays we can shrink the adapters to the USB stick
        - It cannot be smaller. It is due to cable has to plug into that particular port right there, and that has to be a certain size
    - All link layers terminate at the network interface card
    - The controller
        - It is is responsible for paying data out, checking for errors and checking for error collision

11. The received and delivered still is the same things at the link layer

12. The adaptor communication
    - You get it from the other side, check it to see if everything is intact, and pass it up to the upper layer

13. The error detection is not 100% reliable
    - we have error detection at virtually every level

14. The multiple access link
    - There are two type of link. Ethernet can work in both type
        - The point to point. You plug into a switch
            - PPP for dial up. It is the exclusive connection between you and one other note. It is not shared
            - It is between the Ethernet switch and host
        - The broadcast. You do not plug into a switch
            - The old fashioned Ethernet
            - The upstream HFC
                - It is the hybrid fiber coaxial
            - The wireless LAN
                - It is the inherently shared

15. The collision is at the broadcast channels
    - It is that we have two or more simultaneous transmissions by those nodes. You transmit at the same time. The transmissions run into each other. So everything you have just transmitted is going to end up being garbled

16. The multiple access protocol
    - It is the distributed algorithm that determine who can transmit and when to transmit
    - The communication use the channel itself. It use the in band. It cannot use the out of band for the **coordination**

17. The MAC protocol taxonomy three broad class
    - The channel partitioning
        - It is to **divide** the channel up. It assign the channel to a specific note. So it is **exclusive** for you to use
        - If it is not used, it is just going to be unused. It is going to go idle
    - The random access
        - We do not divide the channel. Everyone has full access to the medium
        - You transmit at any time
        - You are going to experience **collisions**
        - You are going to allow those **collisions**
        - You are going to allow those **transmissions** to run into each other periodically
        - you are going to **recover** from those collisions
    - The taking turn
        - It is the alternating protocols
        - Everyone gets their opportunity to transmit. They have to take turn

18. The channel partitioning
    - The TDMA time division multiple access
        - It is the straightforward way of channel partitioning
        - The node is either not participating or it does not have anything to send, it goes unused and it goes idle
        - The **benefits** of TDMA
            - It is that when it is your turn to transmit, you have full access to that entire medium
        - The **problem** of  the DMA
            - It does not scale
            - it goes idle
        - You cannot transmit at any time
    - The FDMA frequency division multiple access
        - It is that we divide up the channel based on frequencies
        - It is that we divide up the spectrum into frequency band
        - You assign the frequency band to the node
        - You can transmit at any time
        - The **problem** of the FDMA
            - The scalability. It does not scale
            - The unused transmission time in the frequency band goes idle 
    - The bandwidth
        - It is the frequency that you can use to transmit
        - It is the amount of data that you can carry
        - It is the width of the frequency you can carry that data on
    - The partitioning problem is the scalability. If it is not using it, it just goes idle

19. The random access
    - The node send data
        - Transmit at full rate
        - no a priority coordination
            - We do not work out who say what and when ahead of time
            - There is collisions
    - The random access protocols is that **collisions** are **not** indicative of **errors**. It is the expected behavior. It is not a problem. It is how the protocol works
    - You are going to have **collisions** all the time
    - The ALOHA is the legacy protocol
        - You randomly picked one of those slots and you transmit
    - The CSMA carrier sense multiple access
        - We use the random access protocol, we have to **detect** the collisions and **recover** from the collisions
            - We use the CSMACD and CSMACA
                - The CSMACD
                    - The CD collision **detection**
                    - We use the CSMACD on **Ethernet** for the collision **detection**
                    - Inorder to increase the efficiency of the CSMA, we add the collision **detection** to it
                    - The collision **detection** is that it will stop transmitting once it detect that there is collision
                    - If the **Nic** has detected another transmission while it was transmitting, it abort. It tell everyone to stop. You're not going to send any useful data during this period after you aborted. After the abort, the NIC enter the exponential backoff
                    - There is the **reliability mechanism** built into this particular protocol
                        - We detect collisions and then we attempt to recover from collisions
                - The CSMACA
                    - The CA collision avoidance
                    - We use the CSMACA on **Wi-Fi** for the collision **detection**
        - we listen before we transmit
            - If channel sense idle, transmit entire frame
            - If channel sense busy, defer transmission. It does not interrupt another node
            - The collisions occur even if listen before transmit
                - Theoretically if you listen before transmit, then you know there is someone talking and you are not going to attempt to talk over them. However, the problem is the propagation delay. So none of these signals will go out to the entire medium at the same time
            - The collision domain is that the shared medium
    - The random access problem is the collision overhead

20. The taking turns alternating protocols
    - It is in **Bluetooth**, but on local area networks we do not see it very **efficiently**
        - The Bluetooth go in sequence and it poll each device to see if there is any data to send
    - Taking turns protocols would kind of be the **best** of both worlds **between** channel partitioning and random access
        - The channel partitioning has this issue where it doesn't scale very well
        - The random access kind of has this issue where there is collision overhead
    - The **random access** protocols actually are much more **efficient**. They actually get data through significantly higher rates than taking turns protocols
    - The categories of the taking turns alternating protocols
        - The polling
            - There is central coordinator. It is the master. The coordinator poll the individual node to check if it has anything to transmit. If yes, it return data. If no, it return the poll and go to the next step. The master go in sequence
        - The token passing
            - There is central device and everyone plug into the central device. You send token along each step in the network. If you have token, you can transmit. If there is nothing to send, you forward the token to the next sequentially

21. The cable access network
    - The CMTS
        - It is the only one that is actually transmitting downstream
        - The problem is that it transmit everything in the broadcast. So every node receive the broadcast. So they have to check the data to identify that if the data is actually destined for them
    - It can trasmit downstream. It cannot transmit upstream. There is collision
        - It is using the **channel partitioning** when it is sending data upstream
    - The download speed is faster than the upload speed if using the cable access network
        - There is time slot and it is handled in random access order. You can transmit upstream during the time slot
        - It is due to how cable access networks divide up the medium





























    






































