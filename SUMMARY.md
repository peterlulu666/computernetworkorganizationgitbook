# Summary

* [Introduction](README.md)
* [Lecture 1 Intro to networking, protocols, the network edge](lecture1.md)
* [Lecture 2 The network core, throughput metrics, TCP/IP protocol stack](lecture2.md)
* [Lecture 3 Security concerns, history, networked applications](lecture3.md)
* [q1](q1.md)
* [Quiz 1](Quiz1.md)
* [Lecture 4 Web browsing, FTP, e-mail](lecture4.md)
* [Lecture 5 DNS](lecture5.md)
* [Lecture 6 Peer-to-peer applications, socket programming, transport-layer requirements](lecture6.md)
* [Lecture 7 Multiplexing : demultiplexing, UDP, intro to reliable data transfer](lecture7.md)
* [q2](q2.md)
* [Quiz 2](Quiz2.md)
* [Lecture 8 Pipelining, TCP](lecture8.md)
* [Lecture 9 TCP](lecture9.md)
* [t1](t1.md)
* [Test 1](test1.md)
* [Lecture 10 Host-to-host communications, virtual circuits versus packet switching, router mechanics](lecture10.md)
* [Lecture 11 IP](lecture11.md)
* [Lecture 12 Routing algorithms](lecture12.md)
* [Lecture 13 Routing on the Internet, broadcast and multicast routing](lecture13.md)
* [q3](q3.md)
* [Quiz 3](Quiz3.md)
* [Lecture 14 Link-layer services, error detection, mutiple access protocols](lecture14.md)
* [Lecture 15 LANs](lecture15.md)
* [Lecture 16 Link virtualization, data center networking](lecture16.md)
* [Lecture 17 Streaming multimedia, CDNs](lecture17.md)
* [q4](q4.md)
* [Quiz 4](Quiz4.md)
* [Lecture 18 VoIP](lecture18.md)
* [t2](t2.md)
* [Test 2](test2.md)















