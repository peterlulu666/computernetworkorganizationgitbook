## Quiz        

1. What are the four sources of packet delay?
    - The processing delay
    - The transmission delay
    - The propagation delay
    - The queuing delay

2. In a large enterprise network, like UTA’s network, why is it necessary to provide so many WiFi Access Points?
    - The system will change address and the access point. So the system is not always on. However, the user perceive that the system is always on. So the user can always access the Internet

3. What is meant by calling network layers “modular”?
    - They have different function and purpose.      
    - It is self-contained and the layer is independent of one another.

4. Why is the Internet of Things (IoT) a problem?
    - It leak the user privacy info.

5. What does the ‘traceroute’ or ‘tracert’ program do?
    - It provide the delay measurement.

6. Why is fiber optic cable immune to electromagnetic interference?
    - It is the light in the glass. So it is immune to electromagnetic interference.

7. In an enterprise network, why do servers and switches almost always connect to the network with wired connections?
    - The enterprise network will use the dedicated bandwidth. It will not use the shared.

8. What do the DNS top-level domain servers track?
    - It track the list of the authoritative server.

9. Briefly describe one benefit of making HTTP is stateless.
    - It is very easy to program. We do not have to do a lot work with the state server. So it make the developer's life easier.

10. If we create our own transport protocol, how do we ensure it is compatible with the public Internet?
    - We do not need to care about it.

11. Why might we want to move data physically closer to end users?
    - It will make the connection faster. The response time has something to do with the physical distance. The user will have slow connection if they are not close to the server

12. What does it mean when HTTP is described as “stateless”?
    - It will not store any info regarding what the client has done if the client get disconnected.

13. What is the purpose of a checksum?  What does a checksum not do?
    - It is the error detection mechanism. It is there to detect error and notify the fail. 
    - It is not there to correct the error.

14. In the context of DNS, what is a “canonical name”?
    - The actual name. The long name. It is something that map to the alias name.

15. One of the ways reliable transport protocols detect errors is the expiration of timers. What is the result if the timer is set too long?
    - It take a long time to identify the problem.

16. Why is TCP so aggressive with its congestion control?
    - The TCP will increase the size of the congestion window by 1. Then every time it receive something correctly, it will gradually increase the window. It will continue until there is loss and it will cut the window in half. It is something like slowly build up and sharply back off. It is aggressive with its congestion control.

17. What is the difference between a multicast and a broadcast?
    - The multicast is the one to many data transmission. It is one sender to multiple receiver.      
    - The broadcast is the one to all data transmission. It is the one sender to every receiver.

18. What is a “cumulative acknowledgement”?
    - The cumulative acknowledgement is that anytime you send the acknowledgement, you are not only acknowledging the packet that you just received, but also every packet in the sequence ahead of it.

19. The ACK number in TCP is indicating what to the receiver?
    - It is the next byte in the sequence that the receiver want to receive.

20. Is it possible for an application to enjoy reliable data transfer even when the application runs over UDP? If so, how?
    - Yes. We can build it up into the application layer.

21. What is the likely result of mixing a UDP flow with a TCP flow?
    - The TCP has the congestion control and the UDP does not have it. So as soon as you turn on the UDP, it will skyrocket to the absolute limit of the capacity and get started sending data as quickly as it possibly can. The transmission will not slow down and it will stay there until you turn it off. The TCP is something like slowly build up and sharply back off. So the graph is something like the jigsaw shape.

22. One of the ways reliable transport protocols detect errors is the expiration of timers. What is the result if timer is set too short?
    - The problem is the premature timeout. It will indicate that there is problem when nothing is wrong.

23. What is an n-way unicast?
    - When a host has to send an exact copy of the same packet to multiple destinations.

24. TCP has a range of bits in its header that are not used for anything.  Why is that unused field there?
    - The creators left space to be used later.

25. We discussed two scenarios in class where using TCP may not be appropriate.  Briefly explain one of those scenarios/
    - If the medium is inherently lossy, we should not use the TCP. The TCP have the congestion control. When the TCP loses the segment, it assumes that it loses the segment due to the congestion. If you are running data over the network, the TCP is great for it. However, let's say that the medium is the air. The air is not the reliable medium. So the medium itself is dropping data. You are lossing segment due to the air. You are not lossing segment due to the congestion. However, the TCP does not know it. The TCP assumes that it loses the segment due to the congestion. So it will be stuck down.
        - If the medium is inherently lossy. We cannot use TCP. TCP have the congestion control. If TCP lose packet, it assume that it is due to the congestion. It is OK to run data over the network. It is not OK if the medium is the air. The air is not the reliable medium. It will drop data itself. You lose packet due to the air. It is not due to the congestion. TCP does not know that. It assume that is lose the packet due to the congestion. So it stuck down.

















