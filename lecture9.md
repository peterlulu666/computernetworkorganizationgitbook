## Quiz      

1. The cumulative acknowledgements
    - The host actually did get both of acknowledgement correctly. Every time you send it, it acknowledges every segment you received ahead of that. 
    - The receiver only acknowledge something is received correctly before a gap. 

2. The TCP **fast retransmit** 
    - Wait for the timer expire. The time out period is long before sending the lost packet. 
    - If the TCP get the duplicate acknowledgements, it means that there is the gap in the sequence, so it assume that it lost segment. 
    - The TCP **fast retransmit mechanism** is that if it receive the same acknowledgment **three times**, it will assume that the packet loss is not caused by the congestion and it will not wait for the timeout period. So it will immediately retransmit the acknowledgement. 

3. The TCP **fast recovery** 
    - The TCP **fast recovery** is that when TCP does fast re-transmit (see above question), instead of beginning a slow-start phase with cwind set to 1, it goes directly to congestion-avoidance phase, with cwind set to ½ its value when packet loss occurred. This is called fast recovery. 

3. The TCP flow control 
    - The received
        - It means that the target system received the data, but it does not dropped off to the application. So it is in this buffer. 
    - The delivered 
        - The application consume the data. 
    - It is possible that something is received, but it is not delivered. 
        - The application may not be ready for it. 
        - The buffer maybe overwhelmed. 
    - The rwnd, receive window, is built into the TCP header. 


4. The build up the connection
    - The handshaking. 

5. The UAPRSF
    - The urgent data. 
    - The ACK. 
    - The PSH. 
    - The RST. 
    - The SYN. 
    - The FIN. 

6. The end to end congestion control
    - When the TCP loses a segment, it assumes that it loses the segment due to the congestion. 
    - Queueing theory is the
mathematical model of waiting in line. In Packet switched networks, everything goes into the queue and wait there. 
    - The TCP make sure that the packet switches are not being overwhelmed. So it does not overwhelm the network. 

7. The network assisted congestion control 
    - Every packet switche that you send to the destination will send a message to the end system. 
    - You're sending a message to the in system, the in system tell you that it is about to overwhelm the capacity and you should slow down. 
    - It is not the scalable solution. It is not practical for the router to send a message to every node to tell them to slow down. 

8. The TCP employees the **additive increase** and **multiplicative decrease** approach to send the data
    - The additive increase
        - The TCP is going to increase the size of the congestion window by 1. And every time it receives something correctly, it will gradually increase the window. It will continue until there is loss and it will cut the window in half. So the window back off sharply. Thereafter, it will slowly build up and back off. So the TCP is aggressive with its congestion control. 
        - The graph is the jigsaw shape. 
        - The queue take a long time to empy, so the window back off sharply.  
    - The multiplicative decrease  

9. The TCP Reno and the TCP Tahoe 

10. When using the TCP is not appropriate? 
    - If the medium is inherently lossy, we should not use the TCP. The TCP have the congestion control mechanism. When the TCP loses a segment, it assumes that it loses the segment due to the congestion. If you are running data over the network, the TCP is great for that. However, let's say that the medium is the air. The air is not the reliable medium. So the medium itself is dropping data. You are losing segment due to the the air. You are not losing segment due to the congestion. However, the TCP does not know that. The TCP assumes that it loses the segment due to the congestion. So it is going to be stuck down. 
    - If it is the high reliability and high throughput link, we should not use the TCP. Let's say that in data center, if you are connecting component to one another. We should use the UDP. The UDP does not have the congestion control mechanism. 






































 






