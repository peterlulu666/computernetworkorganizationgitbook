## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1rQ9R3DwcWLQWfYJbvDpmK3H0Ey-als8M/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1pNWeAcpWt3ScehSH1FRpWNI-SD7PyOin/preview" width="640" height="480"></iframe>

## Quiz        

1. The multimedia networking
    - The multimedia **audio** is the analog audio signal sampled at constant rate
        - The **sampling** rate determine the audio **quality**
            - The number of samples determine how high the quality is
        - The analog is the sound itself
        - The **higher** quality require more **data** and **space**
            - The **higher** quality require us to take a lot of samples and it is the data point. The data takes space

2. The balances to make in the multimedia
    - If we want higher quality multimedia, it require more data and more overhead
    - If we can tolerate lower quality and do not use the highest quality, we can reduce the amount of data that we have to send over the network

3. The multimedia **video** is the sequence of images displayed at constant rate
    - The CBR constant bit rate
        - The video encoding rate fixed
        - The **streaming** multimedia
    - The VBR variable bit rate
        - The video encoding rate changes as amount of spatial, temporal coding changes

4. The multimedia type
    - The **streaming**, **stored** multimedia
        - We **download** the file. We encode it. We **play** it before we downloaded the entire file
        - The **streaming**: at this time, client playing out early part of video, while server still sending later part of video
        - The **streaming** is that we are in the process of retrieving the entire file and we play it while retrieving
        - The **stored** can transmit faster than audio/video will be rendered
    - The **conversational** multimedia
        - It is the dialogue. It is the people talking back and forth. There is the stream of data go to both directions
    - The **streaming** live multimedia
        - It is the broadcasting as it happens

5. The **conversational** is the dialogue and it is the back and forth conversation. The **streaming** is the data go to one directions

6. The Streaming stored video
    - The people do not want to download the entire file before they watch it
    - We stream the video out at a constant rate
    - We send it at constant frame rate
    - The client receive it at a constant rate

7. The jitter
    - It is the difference in latency between one time and another time
    - The network delays are fluctuating
    - There is **client side buffer** to match the requirement
        - We can increase the **client side buffer** in order to conceal the jitter

8. The Streaming stored video problem
    - The client interactivity
        - The users can pause, skip, and go backward and go forward
    - It is the packet switch network, so there is packets lost and retransmitted

9. The performance measure for streaming video is the **average throughput**
    - In order to provide continuous playout, the network must provide an average throughput to the streaming application that is at least as large the bit rate of the video itself
    - The buffering
        - We do not want to overflow the buffer. If we overflow the buffer, then even if we received the data, we are going to lose it due to there is not enough space
        - The client buffering
            - The client side buffering can absorb variations in server-to-client delay
            - If the server-to-client bandwidth briefly drops below the video consumption rate, a user can continue to enjoy continuous playback
    - The prefetching

10. The prerecorded video is stored on, and streamed from, a **CDN** rather than from a single data center

11. There are also many P2P video streaming applications for which the video is stored on users’ **hosts** (peers), with different chunks of video arriving from different peers that may spread around the globe

12. The streaming multimedia UDP
    - the UDP does not **transit** a network any faster than anything else, it just **sends** data faster
    - There's no flow control and no congestion control. We can send data as fast as it can. We can send as much as it can
    - The problem is that recovering from error
        - The error **recovery**: build it into the application level
    - The problem is that the UDP cannot go through **firewalls**
        - The **firewalls** does not send the UDP and does not forward the UDP
            - The UDP cannot go through **firewalls** unless the **firewalsl** is configured to allow it
            - There is not very much UDP traffic on the network. It is restrained due to it cannot go through **firewalls**
        - It cause congestion
        - There is less security
    - The problem is that due to the unpredictable and varying amount of available bandwidth between server and client, constant-rate UDP streaming can fail to provide **continuous** playout
    - The problem is that it requires a media **control** server
        - This increases the overall cost and complexity of deploying a large scale video on demand system

13. The streaming multimedia HTTP
    - The multimedia file retrieved via HTTP GET
        - We do not have these really fancy sophisticated libraries to set up some kind of a streaming multimedia player. It is just HTTP
    - It send at maximum possible rate under TCP
        - The **benefit** for thr TCP is reliable. So if we lose something on the network and the packet disappear, the TCP is going to take care of it for us
    - It fill rate fluctuates due to TCP congestion control, retransmissions. It is the **in order** delivery
        - There is **in order** delivery in the TCP. We play the movie. We want to play the frames in the correct sequence. We have to make sure that those segments are being pushed up to the application layer in order. It is important
    - The larger playout delay: smooth TCP delivery rate
    - The HTTP/TCP passes more easily through **firewalls**
        - The **benefit** using HTTP is that HTTP can go through **firewalls**

14. The DASH dynamic adaptive streaming over HTTP
    - It is the streaming services
        - It allow clients with different Internet access rates to stream in video at different encoding rates
        - It allow a client to adapt to the available bandwidth if the end-to-end bandwidth changes during the session
    - The server
        - It divide the file into chunks
        - The chunks is encoded at the different rate
            - You have the entire video. So you have copies of the video. So each copies have the different video quality
            - If the network connection is slow, choose the lower quality
        - The manifest file
            - It is the file filled with URLs. The URL is for the chunk. So the chunk can receive its own URL
    - The client
        - It **measures** the bandwidth. If the bandwidth is not high, it **consult** manifest to switches the URL and **requests** a lower quality chunk
        - It can dynamically choose the quality and coding rate at different time
        - It chooses maximum coding rate sustainable given current bandwidth
    - The “intelligence” at client: client determines
        - It is the dynamic. So we want to push the decision making to the client
        - The client make the decisions based on the network condition
            - when to request chunk (so that buffer starvation, or overflow does not occur)
            - what encoding rate to request (higher quality when more bandwidth available)
            - where to request chunk (can request from URL server that is “close” to client or has high available bandwidth)

15. The content distribution networks
    - The single, large “mega-server”
        - The straightforward approach to providing streaming video service is to build a single massive data center, store all of its videos in the data center, and stream the videos directly from the data center to clients worldwide
            - The long path to distant clients
                - The problem is that if the **client is far from the data center**, server-to-client packets will **cross many communication links and likely pass through many ISPs**, with some of the ISPs possibly located on different continents. If one of these links provides a throughput that is less than the video consumption rate, the end-to-end throughput will also be below the consumption rate, resulting in **annoying freezing delays** for the user
            - The multiple copies of video sent over outgoing link
                - There is large amount of clients all watching the same video. So there is a lot of redundancy sent over outgoing link
                - The problem is that the popular video will likely be sent many times over the same communication links. Not only does this waste network **bandwidth**, but the Internet video company itself will be **paying** its provider ISP
            - Anytime we have centralization, there is the **single point of failure**
                - The problem is that the single data center represents a **single point of failure**. If the data center or its links to the Internet goes down, it would not be able to distribute any video stream
        - The point of network congestion
        - It does not scale

16. The content distribution networks
    - The Content Distribution Networks CDN
        - We use the CDN to handle large amounts of video data
        - It store/serve multiple copies of videos at multiple geographically distributed sites
        - A CDN manages server **locate** in multiple locations and **store** copies of the videos. It direct each user request to a CDN location that will provide the best user experience
        - The enter deep: push CDN servers deep into many access networks
            - There are a lot of location
            - There are the relatively small data center
            - We want to ensure that the **users** are **closed to** the locations from where they are by going through a minimum amount of distance
        - The bring home: smaller number (10’s) of larger clusters in POPs near (but not within) access networks
            - It is building large clusters at a smaller number of key locations and connecting these clusters using a private high speed network
            - The large clusters near the presence is the short hop to get the high throughput links that can then carry the data
            - There is lower maintenance and management overhead
            - There is higher delay and lower throughput to end users
    - The CDN: “simple” content access scenario
        - The DNS is involved
        - <img src="img/content access scenario.png" alt="content access scenario" width="500" height="600">
        - <img src="img/content access scenario illustrate.png" alt="content access scenario illustrate" width="500" height="600">
        - <img src="img/content access scenario illustrate DNS.png" alt="content access scenario illustrate DNS" width="500" height="600">

17. The CDN cluster selection strategy
    - Let client decide - give client a list of several CDN servers
        - The client ping the list of CDN server
        - The client receive the response and choose the server
        - The client stream from the server
    - In the presence of congestion, the client decrease the quality of the video
    - When the congestion alleviates, the client can choose the higher quality of video to start streaming
    - If the server is not working, the client can pick a different server
    - Hot to pick
        - It pick CDN node geographically closest to client
        - It pick CDN node with shortest delay (or min # hops) to client (CDN nodes periodically ping access ISPs, reporting results to CDN DNS)
        - The IP anycast





















    



















