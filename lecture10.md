## Lecture      

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1EqiN2jXD8pfQ0w2w2k4U_eXaGdNezcDQ/preview"></iframe>

## Lecture Video        



<iframe src="https://drive.google.com/file/d/1McG3Ejns-AClgEaHXyaDuVb_vtpLtoNx/preview" width="640" height="480"></iframe>

## Quiz        

1. The network layer service models 
    - It is like quality of service at the transport layer.      

2. The network layer        
    - It is running on every host.      

3. The router
    - The job is to forward traffic.        
    - You push data to the router. It will send the data somewhere else.        
    - It is a relay. It is not necessarily used as a targeted end system.     

4. The forwarding and the routing
    - The forwarding is that it physically moving the packets from one port to another port.        
    - The routing is that it determines the best path through the network.        

5. The network is between two host. The transport is between to process.      

6. The virtual circuits
    - If you will offer quality of service at the network layer, you have to set it up.        
    - The **service models** is for the virtual circuits.       
        - The quality of service at the network layer, we call it the **service models**.      
            - We will divide up that medium based on a channel.      
            - The channel is the ways of partitioning up the medium.        
            - The channel is going to be capable of physically handling different data streams at the same time.      
            - The service model for channel 
                - It guarantee in order delivery
                - It guarantee minimum bandwidth        
    - The UDP is **connectionless** service and the virtual circuits is **connection** service.        
        - In **UDP**, you do not set anything up, you do not agree upon anything ahead of time, and you do not send notification.        
        - In **virtual circuits**, you can set up a connection ahead of time. So you can make certain guarantees.        
    - The **circuit**
        - We will go the entire source to destination path.        
        - Every time we go across a router and every time we made a particular hop, we have to stop at the hop and set aside resources for the connection. You have to perform it from the **end system** to the **in system** and from the **in system** to the **end system**.        
    - The **virtual**
        - All of the mediums that exist in the routers is shared between multiple connections. We cannot use the cable to physically set up the connection.        
    - The virtual circuits implementation
        - It consist of      
            - path from source to destination
            - virtual circuit number        

7. The Internet is the **best effort** system
    - It is the one effort. It try once.        
    - It is the packet switched network.      
        - There is no bandwidth guarantee, no delivery guarantee, no order guarantee, and no timing guarantee.      

8. The Datagram Network
    - There is no set up at the network layer. It is like UDP.      

9. The longest prefix matching
    - When looking for forwarding table, use the longest prefix matching to match the destination address.      

10. The switching via memory

11. The switching via bus
    - The bus contention
        - It is the conflict on that bus.        

12. The switching via interconnection network

13. The **buffer required**
    - The switching fabric is fast. So it exceed the rate at which we can pay that out to the medium. So we physically cannot put the bit onto the medium fast enough to keep up with the switching fabric.      

14. We have **queuing** at both the **input port** and the **output port**, but we lose the traffic at the **output port**.      

15. The output port queuing
    - buffering when we physically cannot put the bit onto the medium fast enough to keep up with the switching fabric.        
    - queuing and loss due to output port buffer overflow.        
        - It is that not only the switching fabric is faster than the medium can be physically made to pay the data out, but we also have a situation where multiple input ports can be emptying into the same output port. So the queuing delay and loss is due to the **output port** buffer overflow.        

16. The input port queuing
    - fabric slower than input port, so the queuing is at the inout queue.        
        - the queuing delay and loss due to **input port** buffer overflow.        

































