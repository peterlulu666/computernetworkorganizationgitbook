## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1SeHZMoq1UXoq4r-Zp3Pixz-Ne-WWpQ0x/preview"></iframe>

## Lecture Video        



<iframe src="https://drive.google.com/file/d/1MdAUeGu95bV0fDT1wuKKggT1zEbMg6VA/preview" width="640" height="480"></iframe>

## Quiz       

1. The MAC addrss and ARP address resolution protocol
    - The Mac address is that this is universally unique
    - It is on the network Interface card
    - The Mac address is 48 bits
    - The 32 bit **IP address** is provided via **DHCP**
    - The static IP address is assigned to node based on the network administrators configuring the address space
    - Your IP address has to be given to you
    - The Mac address is not going to be routed to the public Internet
    - The LAN address is the Mac address
    - The Mac address is the flat address
    - You can bring the MAC address with you

2. The LAN
    - The 48 bit address
    - Your subnet mask has to tell you which part of that address is which
    - You cannot bring the IP address with you. If you connect into another network, there is different set up for the address space

3. The ARP table Maps IP addresses onto Mac addresses

4. The link layer broadcasts is the way you locate who is on the subnet

5. The link layer broadcast a broadcast address is always going to be all ones in the destination field

6. You can set up your device to just not respond to the broadcast ARP queries

7. The plug and play protocol
    - If you don't do anything specific to disable these settings or to change these settings, this is all going to happen.
Based on the protocol. You do not have to set anything up. You just connect the Ethernet cable to the computer. The computer is going to automatically start sending the broadcasts and building up a table of who is present and who is on the network

8. The Ethernet
    - The Wi-Fi network interface card is the Wi-Fi access point. After the Wi-Fi access point, it use the wired connection to go to the router or to go into another switch
    - The Ethernet is essentially synonymous with CAT5 or CAT6 cables. However, the Ethernet is not the cable
    - The Ethernet is not the cable, it is the link layer protocol that dictates how we put data onto the cable
    - The bus coaxial cable
    - The star switch

9. The Ethernet frame structure
    - The receiver have to examine the incoming frames to make sure that they are destined for that particular network interface card
    - The Ethernet is going to assume that it is pushed out over a shared medium and shared mediums have this broadcast
    - The shared medium is the broadcast. Every node connecting to the medium is receiving every frame
    - The CRC cyclic redundancy check at receiver
        - It is the error detection
    
10. The Ethernet is unreliable and connectionless
    - It is the **connectionless**. There is no handshaking between sending and receiving NICs
    - It is the **unreliable**. It does not have the acknowledgements or negative acknowledgements inherently built into the protocol
        - The Ethernet is **unreliable** if the data are **corrupted**. So if something transits that medium and it is corrupted during the transit, the Ethernet just drops. It doesn't attempt to fix it
    - Ethernet is going to **recover** from collisions, but it is still not reliable
        - The CSMACD recognize there is the collisions. It will back off and it will resend the data that lost due to the collision
    - There is the copper physical layer and the fiber physical layer

11. The cable color coded
    - The cables are woven together
    - The color coded pairs are twisted together
    - It is due to that they are the copper wires. So there is signal travels down that wire. So there is signal travels out that wire. If the wires is not twisted, there is the electromagnetic interference. In order to eliminate the electromagnetic interference, the wires is twisted

12. The category 6 works the same way. The difference with Category 6 is that category 6 has the insulator that runs down the center of the cable. The insulator that runs down the center of the cable further reduce the electromagnetic interference. So there is high throughput speeds going through the medium

13. The Ethernet switch
    - The bridge is the primitive way of partitioning pieces of network from one another
    - The switches take an active role in handling traffic
    - The store and forward Ethernet frame
        - It get the entire frame and make a forwarding decision based on the entire frame. It send the frame along to where it needs to go
    - The host have the dedicated and direct connection to the switch. It eliminates the collision and it is the full duplex
    - The switch know the reachable
        - The switch have the forwarding table that use the Mac addresses
    - The self learning switch
        - There is initial frame sent through the switch. It create the first entry. In order to know where to forward the frame, the switch send the frame to everyone and wait for the response. If the switch receive the response, it use the forwarding table to find which interface to forward and selectively send on just one link. It partition the collision domain automatically
    - The switch can be connected together
    
14. The institutional network
    - The Wi-Fi access point is going to plug into a switch
    - The mail server and the web server is going to plug into a switch
    - The switch is going to partition the address space
    - The router does not deal with any traffic. It does not go off to the Internet
    - The switch help the router reduce the congestion and the overhead

15. The switch and the router store and forward
    - They both use store and forward
        - They have to get the entire frame before they actually begin to make that forwarding decision based on the content of the frame
    - The router is going to use the network layer header
    - The switch is going to use the link layer header

16. The switch and the router forwarding table
    - They both use forwarding tables
        - You have to attach a range of addresses to the interface in order to figure out where you forward the data
    - The router use the routing algorithm to compute the forwarding table 
    - The switch is going to self assemble the forwarding table

17. The switch works up to layer 2, the data link layer. The router works up to layer 3, the network layer

18. The VLAN virtual local area networks
    - We can set up the virtual local area network for group of people. It is important for the security
    - The local
        - Your desktop system is going to be plugged into the same piece of networking equipment as the desktop system of whoever is working next to you. You are connecting to the the same piece of equipment
    - The VLAN allow us to partition the networks and have the control
    - The VLAN allow you to treat the one piece of networking equipment as if it is two different pieces of networking equipment
        - The sharing file in the group
    - The traffic isolation
        - It is rather than have this range of ports that are built up, we can have dynamic membership
    - The dynamic membership
        - It is that dynamically assign port in the VLAN. It is virtually assign interface in the workstation

        

    


















    


















