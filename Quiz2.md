## Quiz      

1. Consider a new peer Alice that joins BitTorrent without possessing any chunks. Without any chunks, she cannot become a top-four uploader for any of the other peers, since she has nothing to upload. How then will Alice get her first chunk?
    - After 30 second, the existing peer will pick someone at random. If they select Alice, they will optimistically unchoke Alice. So Alice will join it and get the first chunk. 

2. What is the most common transport protocol used by routers? 
    - There is no one. 

3. One reason to use a web cache is to decrease latency. How does using a web cache decrease latency? 
    - It will reduce response time for the client request. 
    - It will reduce load request for the server. 
    - It will reduce traffic on the link. 

4. What does it mean when HTTP is described as “stateless”? 
    - It does not store the info regarding what the client has done if the client get disconnected. 

5. How does DNS balance load across a system’s replica servers? 
    - It iterates (e.g., cycles through) the results it returns to lookup queries, so that traffic is spread out between replicas. 

6. What is the purpose of a checksum?  What does a checksum not do? 
    - It is the error detection mechanism. It will detect the error and notify the fail. 
    - It does not correct error. 

7. In the context of DNS, what is a “canonical name”? 
    - The acual name. The name that is very long. It will map to the alias name. 

8. What do the DNS top-level domain servers track? 
    - It track the list of the authoritative server. 

9. Briefly describe one benefit of making HTTP is stateless. 
    - It is very easy to program. We do not have to do a lot of work with the state server. 
    - It make the developer's life easier if they do not have to maintain the state. 

10. If we create our own transport protocol, how do we ensure it is compatible with the public Internet? 
    - We don’t need to. 
    - If you want to make the system available on the public Internet
        - You should set up the authoritative DNS server. 

11. Why does DNS present a privacy risk? 
    - It will leak private info, it will be manipulated, and it will have a record of everywhere you go.  
    - Everywhere you go on the Internet has to be resolved via DNS, so DNS has a record of everywhere you go. 

12. What is the purpose of a DNS type A record? 
    - The IP address for the authoritative server host name. 
    - It maps the URL to the actual IP address. 

13. How does UDP signal that a segment has been lost? 
    - The UDP does not notify it. 

14. In the context of P2P, what is meant by the term “churn”? 
    - The peers come and go – they will not constantly be on the network. 
    - The peers join and leave the network. 

15. Name two quality-of-service (QoS) guarantees that TCP does not offer. 
    - There is no delay guarantee and the bandwidth guarantee. 

16. There are multiple problems with centralizing services.  Name two of them. 
    - If we have one server and there is something wrong with it, the service on the server will be stoped. We have to wait for the server to get back. 
    - If we have one server, there is a lot traffic going into the server. The traffic link will be overloaded and the system will be overloaded. So it will not handle all the traffic at once. 
    - Let's say that there is one server. 

17. FTP is said to send control data “out of band”.  What is meant by saying data are sent “in band”? 
    - It use the single TCP connection sending data. So there is one TCP connection. 
    - Data are sent over a single TCP connection. 

18. IMAP and POP3 are two common protocols user agents utilize to access mailboxes.  What is the major difference between those two protocols? 
    - The IMAP will store the mail on the server. 
    - The POP3 will not store the mail on the server. The POP3 will download the mail and erase the mail. 

19. Why might we want to move data physically closer to end users? 
    - In order to get the connection faster. The response time has something to do with the physical distance. The user will get slow connection if they are not close to the server. 

20. Why is a “length” field necessary in a UDP header? 
    - There is different amount of payload. So it will grow and shrink. So we will notify the receiver the amount of data in the segment. 


    






















