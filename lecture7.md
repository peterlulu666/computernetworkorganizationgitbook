## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Lt8jHr5-m0OgIYKNXIh2_Gf2Dhg6yyx9/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1EcWwX2hoctwWjJo1Alvu79nxhgB6tiCB/preview" width="640" height="480"></iframe>

<iframe src="https://drive.google.com/file/d/1avxbUfL74K8FcKD2moDfJvbth8S-sLgP/preview" width="640" height="480"></iframe>

## Quiz      

1. The multiplexing
    - We take multiple stream of info and combine them together.      

2. The demultiplexing
    - we use the header info to deliver data to socket.      

3. The host use the IP address and port number to send data to the socket
    - We have to have the port number. The system will have multiple processes with active TCP connections running on it at the same time. The port number will help the system to identify the process that will receive the data.           

4. The TCP and UDP header start with the source port number and the destination port number.              

5. The connectionless demultiplexing
    - The UDP is connectionless
        - When transfer the data, the UDP will only use the destination IP address and the destination port number.       
        - If the two UDP have different source IP address, but they have the same port number, they will go into the same socketat the destination. The destination application will idetify the purpose of the data.           

6. The well known port number is 1024.      

7. The TCP connection
    - The destination use 
        - source IP address.
        - source port number.
        - destination IP address.
        - destination port number.

8. The UDP have one socket for everything. 

9. The TCP have different socket for every data stream. The transport layer will help the application to idetify the purpose of the data.      

10. The UDP best effort
    - The best effort is the one effort. It will try one time.
    - The data maybe received out of order, the data maby lost, the UDP does not provide notification.      

11. The UDP is not reliable
    - We should build up the reliability mechanism into the application layer.      

12. The UDP benefit
    - It is simple. It is connectionless. It transmit fast.
    - The UDP is not for data integrity transfer.      

13. The UDP connectionless
    - It is for transfering small amount of data.        
        - The DNS

14. The UDP length
    - We can put different amount of payload, so it can grow and shrink. So we have to notfy the receiver how much data is in the segment.       

15. The UDP checksum
    - It is the error detection mechanism.   
    - It does not correct error.    
    - It does notify the fail.      

16. The reliable transfer in the reliable channel
    - The application layer rdt_send()
    - The transport layer udt_send()

17. The channel error
    - The recover from error.  
        - acknowledgement, if the checksum detect no error, it notify OK.
        - negative acknowledgement, if the checksum fail, it notify error.      

18. The sender handle duplicate
    - The sender add sequence number to the packet. 
    - The stop and wait.
    - We only use two sequence number. We only send one packet at a time.      

19. The NAK free protocol
    - Instead of sending the negative acknowledgement, we only send acknowledgement for the last packet received.        

20. The sender will set the timeout.  
    - The short timeout setting, The problem is the premature timeout. It will indicate there is problem whe nothing is wrong.      
    - The long timeout setting, it takes long time to identify the problem.        































